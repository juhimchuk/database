

DECLARE @doc xml 
SET @doc = '<Client>
 <Item ClientName="Jaroslav" ClientSurname="Juh" ClientAge="22"/>
 <Item ClientName="Maria" ClientSurname="Juh" ClientAge="45"/>
 <Item ClientName="Vita" ClientSurname="Juh" ClientAge="66"/>
   </Client>' 

Exec ModifyClients @doc


CREATE PROCEDURE ModifyClients (@some xml) 
AS
Begin
begin tran
DECLARE @xmlDoc integer
EXEC sp_XML_preparedocument @xmlDoc OUTPUT, @some
begin try
declare @t1 table (ClientName nvarchar(50), ClientSurname nvarchar(50),ClientAge int)
insert into @t1
select * from 
OPENXML(@xmlDoc,'Client/Item',1)
with
(ClientName nvarchar(50),
ClientSurname nvarchar(50),
ClientAge int )

merge into Client using  @t1 as sourse
on sourse.ClientName=Client.ClientName
WHEN MATCHED THEN
	UPDATE SET Client.ClientSurname = sourse.ClientSurname, Client.ClientAge = sourse.ClientAge
WHEN NOT MATCHED BY TARGET THEN
	INSERT  VALUES (sourse.ClientName,sourse.ClientSurname,sourse.ClientAge);
Exec sp_XML_removedocument @xmlDoc
end try
begin catch
Exec sp_XML_removedocument @xmlDoc
rollback
end catch
commit
end

