Select CategoryName,UnitPrice
From Categories
INNER JOIN Products
 ON Categories.CategoryID = Products.CategoryID
 Where UnitPrice =(Select Max(UnitPrice)   From Products
 where Categories.CategoryID = Products.CategoryID)