SELECT  Region,City,COUNT(*) AS [Count]
 FROM Suppliers
 Where Country = 'Sweden'
 GROUP BY Region,City
  HAVING COUNT(*) > 1