Select FirstName,LastName,TerritoryDescription,RegionDescription
From Employees
INNER JOIN EmployeeTerritories
 ON Employees.EmployeeID=EmployeeTerritories.EmployeeID
 INNER JOIN Territories
 ON EmployeeTerritories.TerritoryID=Territories.TerritoryID
 INNER JOIN Region
 ON Territories.RegionID=Region.RegionID