

INSERT INTO [User](name,[password]) VALUES ( 'Mike', '123456');
INSERT INTO [User](name,[password]) VALUES ( 'Nike', 'qwertyui');
INSERT INTO [User](name,[password]) VALUES ( 'Jaroslav', 'viet152');
INSERT INTO [User](name,[password]) VALUES ( 'Xamarin', '123ert');
INSERT INTO [User](name,[password]) VALUES ( 'Prog', 'prog157');
INSERT INTO [User](name,[password]) VALUES ( 'zinia', 'zinia8345');


INSERT INTO [Category](id_user,name) VALUES ( '1', 'Home_work');
INSERT INTO [Category](id_user,name) VALUES ( '1', 'Bussines_work');
INSERT INTO [Category](id_user,name) VALUES ( '2', 'Home_work');
INSERT INTO [Category](id_user,name) VALUES ( '2', 'Study');
INSERT INTO [Category](id_user,name) VALUES ( '3', 'Rest');
INSERT INTO [Category](id_user,name) VALUES ( '4', 'Study');
INSERT INTO [Category](id_user,name) VALUES ( '5', 'Rest');


INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '1', '�leaning','1','It is necessary to clean the house.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '1', '�ooking','3','Need to cook dinner.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '2', 'Make a report','2','It is necessary to compile a monthly report.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '3', '�leaning','1','It is necessary to clean the yard.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '5', 'Camping','2','Camping in the woods with friends.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '6', 'Databases','2','Learning the basics.');
INSERT INTO [ToDoList](id_category,name,[priority],[description]) VALUES ( '6', 'Net','1','Learning the basics.');



INSERT INTO [Task] (id_todolist,name,[description],[priority],[status],date_creation,date_finish) VALUES ( '8', 'Clean kitchen','To wash the dishes','1','planned','2017-09-30','2017-10-05');
INSERT INTO [Task](id_todolist,name,[description],[priority],[status],date_creation,date_finish) VALUES ( '8', 'Clean bedroom','To wash the floor','2','planned','2017-09-30','2017-10-05');
INSERT INTO [Task](id_todolist,name,[description],[priority],[status],date_creation,date_finish) VALUES ( '9', 'Cook dinner','Fry potatoes','1','planned','2017-09-30','2017-09-30');
INSERT INTO [Task](id_todolist,name,[description],[priority],[status],date_creation,date_finish) VALUES ( '10', 'Clean yard','Cut the lawn','5','planned','2017-09-30','2017-10-04');
INSERT INTO [Task](id_todolist,name,[description],[priority],[status],date_creation,date_finish) VALUES ( '11', 'Buy a tent','Tent for 4 people','2','planned','2017-09-30','2017-10-10');
INSERT INTO [Task] VALUES ( '12', 'Designing relational databases.','Explore the concept of databases','1','planned','2017-09-30','2017-10-17',24);
INSERT INTO [Task] VALUES ( '13', 'Writing data to xml-file.','Explore the concept of serealization','1','planned','2017-09-30','2017-10-17',24);
