
 Select DISTINCT Region.RegionID,Region.RegionDescription 
 From Region
 Left JOIN Territories
 ON Region.RegionID=Territories.RegionID
  Left JOIN EmployeeTerritories
  ON Territories.TerritoryID=EmployeeTerritories.TerritoryID
   Where EmployeeID IS NULL
