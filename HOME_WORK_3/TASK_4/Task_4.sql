CREATE VIEW view_ToDoList
    AS SELECT *
    FROM ToDoList;

CREATE TRIGGER tr_inserd_view_ToDoList
ON view_ToDoList
CREATE VIEW view_ToDoList
    AS SELECT *
    FROM ToDoList;

 CREATE TRIGGER tr_inserd_view_ToDoList
ON view_ToDoList
INSTEAD OF INSERT
AS Begin
SET NOCOUNT ON
DECLARE @name nvarchar(50),
        @priority int,
        @description nvarchar(max),
		@table_name nvarchar(255),
		 @LocationTVP AS TableTypes;
Select @table_name=N'ToDoList'
SET @name=(SELECT [Name] FROM inserted)
SET @priority=(SELECT [Priority] FROM inserted)
SET @description=(SELECT [Description] FROM inserted)
INSERT INTO @LocationTVP values(@name,@priority,@description)
EXEC usp_InsertTasks @LocationTVP,@table_name;
end

INSERT INTO view_ToDoList([Name],[Priority],[Description])  values(N'qwe',5,N'das')